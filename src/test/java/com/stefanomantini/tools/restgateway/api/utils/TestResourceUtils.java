package com.stefanomantini.tools.restgateway.api.utils;

import com.google.common.io.Resources;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public final class TestResourceUtils {
  public static final String DEFAULT_RESOURCE_DIR = "/wiremock";

  private TestResourceUtils() {}

  public static String readResource(String url) {
    return readResource(url, "/wiremock");
  }

  public static String readResource(String url, String prefix) {
    try {
      return Resources.toString(
          TestResourceUtils.class.getResource(prefix + "/" + url), StandardCharsets.UTF_8);
    } catch (IOException var3) {
      throw new RuntimeException(var3);
    }
  }
}
