package com.stefanomantini.tools.restgateway.service.impl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import com.stefanomantini.tools.restgateway.service.contract.ProfileService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class ProfileServiceTest {

  @Autowired ProfileService profileService;

  @Test
  public void testGetPrefixedProfiles() {
    profileService.getPrefixedProfiles("prms");
  }
}
