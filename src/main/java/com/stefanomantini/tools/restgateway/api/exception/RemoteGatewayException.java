package com.stefanomantini.tools.restgateway.api.exception;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class RemoteGatewayException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  /**
   * Construct RemoteGatewayException with message and cause.
   *
   * @param message
   * @param cause
   */
  public RemoteGatewayException(String message, Throwable cause) {
    super(message, cause);
  }

  /**
   * Construct RemoteGatewayException with message.
   *
   * @param message
   */
  public RemoteGatewayException(String message) {
    super(message);
  }

  @Override
  public String toString() {

    return new ToStringBuilder(this)
        .append("message", this.getMessage())
        .append("cause", this.getCause())
        .toString();
  }
}
