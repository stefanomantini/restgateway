package com.stefanomantini.tools.restgateway.service.impl;

import com.stefanomantini.tools.restgateway.service.config.ServiceConstants;
import com.stefanomantini.tools.restgateway.service.contract.ConfigurationService;
import com.stefanomantini.tools.restgateway.service.contract.ProfileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ProfileServiceImpl implements ProfileService {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @Autowired Environment env;

  @Autowired
  ConfigurationService configurationService;

  @Override
  public List<String> getPrefixedProfiles(String prefix) {
    String regex =
        "(?:" + prefix + "-)(.*?)(?:" + ServiceConstants.SPRING_PROFILE_FILE_FORMAT + ")";
    Pattern pattern = Pattern.compile(regex);
    List<String> prefixedProfiles = new ArrayList<>();
    for (String fileName :
        configurationService.getResourceFileNames(
            ServiceConstants.SPRING_PROFILE_CONFIG_LOCATION)) {
      Matcher matcher = pattern.matcher(fileName);
      if (matcher.find()) {
        prefixedProfiles.add(prefix + "-" + matcher.group(1));
      }
    }
    return prefixedProfiles;
  }
}
