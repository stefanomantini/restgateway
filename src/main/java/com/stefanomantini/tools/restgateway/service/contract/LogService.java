package com.stefanomantini.tools.restgateway.service.contract;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public interface LogService {

  /**
   * Logs proxy requests, nothing fancy
   * @param request
   * @param headers
   * @param body
   * @param context
   * @param requestResponseId
   */
  void logRequest(
      HttpServletRequest request,
      HttpHeaders headers,
      String body,
      String context,
      String requestResponseId);

  /**
   * Logs proxy responses, nothing fancy
   * @param result
   * @param requestResponseId
   */
  void logResponse(ResponseEntity<String> result, String requestResponseId);
}
