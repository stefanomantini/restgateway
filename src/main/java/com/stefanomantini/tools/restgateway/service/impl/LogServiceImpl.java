package com.stefanomantini.tools.restgateway.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Joiner;
import com.stefanomantini.tools.restgateway.service.contract.LogService;
import org.apache.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class LogServiceImpl implements LogService {

  private final Logger log = Logger.getLogger(this.getClass());

  @Override
  public void logRequest(
      HttpServletRequest request,
      HttpHeaders headers,
      String body,
      String context,
      String requestResponseId) {
    log.debug("********************************");
    log.debug("Request ************************");
    log.debug("ID: " + requestResponseId);
    log.debug("Request Time: " + LocalDateTime.now().toString("yyyy-MM-DD hh:mm:ss"));
    log.debug(request.getMethod() + " Request to: " + context);
    try {
      ObjectMapper mapper = new ObjectMapper();
      log.debug("Start Payload: *****************");
      log.debug(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(body));
    } catch (JsonProcessingException e) {
      log.debug("Headers: ***********************");
      Joiner.MapJoiner mapJoiner = Joiner.on(",").withKeyValueSeparator("=");
      log.debug(mapJoiner.join(headers.toSingleValueMap()));
    }
    log.debug("");
  }

  @Override
  public void logResponse(ResponseEntity<String> result, String requestResponseId) {
    log.debug("********************************");
    log.debug("Response ***********************");
    log.debug("ID: " + requestResponseId);
    log.debug("Time: " + LocalDateTime.now().toString("yyyy-MM-DD hh:mm:ss"));
    log.debug(result.getStatusCode());
    log.debug("Payload: ***********************");
    log.debug(result.getBody());
    log.debug("Headers: ***********************");
    Joiner.MapJoiner mapJoiner = Joiner.on(",").withKeyValueSeparator("=");
    log.debug(mapJoiner.join(result.getHeaders()));
  }
}
