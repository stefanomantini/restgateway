package com.stefanomantini.tools.restgateway.service.contract;

import java.util.List;

public interface ProfileService {

  List<String> getPrefixedProfiles(String prefix);
}
