#!/usr/bin/env perl
use strict;
use warnings;
package Verify;
# # # # # # # # # # LIBRARIES

# # # # # # # # # # CONFIG
# # # # # # # # # # VARIABLES
my $REPO;
my $ANT_OPTS;

# # # # # # # # # # SUB DECLARATIONS
sub setup(%);

# # # # # # # # # # MAIN
sub call(%){
    my %in = @_;
    setup(@_);
    chdir $REPO;

    if($in{jenkins}){ # Jenkins only checks
    }
}
1;
# # # # # # # # # # SUBS
sub setup(%){
    my %in = @_;
    *logg = $in{subs}{logg};
    *addError = $in{subs}{addError};
    *addWarning= $in{subs}{addWarning};
    *addInfo = $in{subs}{addInfo};
    *callout = $in{subs}{callout};
    $REPO = $in{repo};
}
